#include "mainwindow.h"
#include "ui_mainwindow.h"
#include"test.h"
#include"testwidget.h"
#include"testdialog.h"

#include<QPushButton>
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
 //////////////////////////////////////////////////////////
    //widget类窗口测试
    //没有父类指定，不是内嵌窗口有独立的边框
#if     1
    testwidget *w=new testwidget;
        w->show();
#else //指定父对象，不是独立窗口，所以跟随父类显示，没有边框
   testwidget *w=new testwidget(this);


 #endif
//    QPushButton* btona=new QPushButton(this);
//    //移动位置，相对父窗口位置
//    btona->move(10,10);
//    //设置按键大小
//    btona->setFixedSize(30,30);



//////////////////////////////////////////////////////////////
   //dialog窗口测试
#if 0

     testDialog* td=new testDialog(this);
     //非模态 show
     td->show();
#else

   testDialog* td=new testDialog(this);
   //模态 exec
   //阻塞程序运行的，需要关闭模态的窗口才能进行切换
   td->exec();
#endif

}

MainWindow::~MainWindow()
{
    delete ui;
}

