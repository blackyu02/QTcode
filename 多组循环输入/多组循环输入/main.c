#include<stdio.h>
//多组输入，一个整数（2~20），表示直角三角形直角边的长度，即“*”的数量，也表示输出行数。
//int main()
//{
//    int n=0;
//    while(scanf("%d",&n)!=EOF)
//    {
//        int i=0;
//        for(i=0;i<n;i++)
//        {
//            int j=0;
//            for(j=0;j<=i;j++)
//            {
//                printf("* ");
//            }
//            printf("\n");
//        }
//    }
//    return 0;
//}

//多组输入，一个整数（2~20），表示翻转直角三角形直角边的长度，即“*”的数量，也表示输出行数

int main()
{
    int n=0;
    while(scanf("%d",&n)!=EOF)
    {
        int i=0;
        for(i=0;i<n;i++)
        {
            int j=0;
            for(j=0;j<n-i;j++)
            {
                printf("* ");
            }
            printf("\n");
        }
    }
    return 0;
}